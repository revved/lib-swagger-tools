import SwaggerParser from "swagger-parser";
import Promise from "bluebird";
import fs from "fs-extra";
import jsYaml from "js-yaml";
import forEach from "lodash/forEach";
import set from "lodash/set";
import split from "lodash/split";
import join from "lodash/join";
import values from "lodash/values";
import startsWith from "lodash/startsWith";
import copy from "recursive-copy";

// This code is also ran from the `dist` dir, so we go up a level, then under `lib`.
const SAMPLE_PATH = `${__dirname}/../sample`;
const COMMON_COMPONENTS_DIR = `${__dirname}/../lib/swagger`;
const COMMON_FILE_PREFIX = "common-";
const PATHS_FILE_NAME = `paths.yaml`;
const COMPONENTS_FILE_NAMES = {
    SCHEMAS: "schemas.yaml",
    REQUEST_BODIES: "requestBodies.yaml",
    PARAMETERS: "parameters.yaml",
    RESPONSES: "responses.yaml"
};

class SwaggerTools {
    async bundleToFile(swaggerSpecFilePath, options) {
        const { outputFilePath } = options;
        if (!outputFilePath) {
            throw new Error("Please provide an output file path");
        }

        const bundledSwagger = await this.bundle(swaggerSpecFilePath, options);

        await fs.writeFile(outputFilePath, JSON.stringify(bundledSwagger));
    }

    async bundle(swaggerSpecFilePath, options) {
        // Convert yaml index to json
        const swaggerSpec = await this.yamlToJson(swaggerSpecFilePath);
        swaggerSpec.components = {};

        // Resolve all components
        const fileNames = values(COMPONENTS_FILE_NAMES);
        // eslint-disable-next-line lodash/prefer-lodash-method
        const componentMaps = await Promise.map(fileNames, fileName => {
            return this.getComponents(swaggerSpecFilePath, fileName);
        });

        // Unpack
        forEach(componentMaps, componentMap => {
            swaggerSpec.components[componentMap.name] = componentMap.components;
        });

        // Add paths
        swaggerSpec.paths = await this.getPaths(swaggerSpecFilePath);

        // Now bundle, we should be able to reference common specs
        const bundledSwagger = await SwaggerParser.bundle(swaggerSpec);
        return bundledSwagger;
    }

    async seed() {
        const opts = {
            overwrite: true,
            expand: true
        };

        await copy(SAMPLE_PATH, `./swagger`, opts);
    }

    // Definitions

    async getComponents(swaggerSpecFilePath, componentFileName) {
        const componentName = split(componentFileName, ".")[0];
        // Get spec dir to get definitions
        const specDir = this.getSwaggerSpecDirPath(swaggerSpecFilePath);
        const componentsPath = `${specDir}/${componentFileName}`;
        const components = await this.yamlToJson(componentsPath);

        // Get common definitions and combine them
        let commonComponents = await this.getCommonComponents(
            componentFileName
        );
        commonComponents = commonComponents || {};

        let allComponents = {
            ...components,
            ...commonComponents
        };

        allComponents = this.getNonAnchorsComponents(allComponents);

        return { components: allComponents, name: componentName };
    }

    getNonAnchorsComponents(components) {
        const nonAnchorComponents = {};
        forEach(components, (component, name) => {
            const shouldInclude = !startsWith(name, "Anchor");
            if (shouldInclude) {
                nonAnchorComponents[name] = component;
            }
        });

        return nonAnchorComponents;
    }

    async getCommonComponents(componentFileName) {
        const commonComponentsPath = `${COMMON_COMPONENTS_DIR}/${COMMON_FILE_PREFIX}${componentFileName}`;
        const commonComponents = await this.yamlToJson(commonComponentsPath);

        if (!commonComponents) {
            return null;
        }
        const commonComponentsPrefixed = {};

        // Prefix `Common`
        forEach(commonComponents, (component, name) => {
            const prefixedName = `Common${name}`;
            set(commonComponentsPrefixed, prefixedName, component);
        });

        return commonComponentsPrefixed;
    }

    async getPaths(swaggerSpecFilePath) {
        const specDir = this.getSwaggerSpecDirPath(swaggerSpecFilePath);
        const pathsPath = `${specDir}/${PATHS_FILE_NAME}`;
        const paths = await this.yamlToJson(pathsPath);

        return paths;
    }

    // Helpers

    getSwaggerSpecDirPath(swaggerSpecFilePath) {
        const parts = split(swaggerSpecFilePath, "/");
        parts.pop();
        const dir = join(parts, "/");
        return dir;
    }

    async yamlToJson(filePath) {
        let buffer;
        try {
            buffer = await fs.readFile(filePath);
        } catch (e) {
            return null;
        }

        const json = jsYaml.safeLoad(buffer);
        return json;
    }
}

export default new SwaggerTools();
