import path from "path";
import fs from "fs-extra";
import { SwaggerTools } from "../lib";

const SAMPLE_SWAGGER_FILE_PATH = `${__dirname}/../sample/src/swagger.yaml`;

test("should bundle swagger with a file path and return object", async () => {
    const bundledSwagger = await SwaggerTools.bundle(SAMPLE_SWAGGER_FILE_PATH);

    // Assert
    assertBundledSwagger(bundledSwagger);
});

test("should bundle swagger with a file path and save to provided file path", async () => {
    const opts = {
        outputFilePath: `${__dirname}/../sample/dist/swagger.json`
    };
    await SwaggerTools.bundleToFile(SAMPLE_SWAGGER_FILE_PATH, opts);

    // Assert
    const bundledSwagger = await fs.readJson(opts.outputFilePath);
    assertBundledSwagger(bundledSwagger);
});

test("should throw error trying to bundle to file with no output file provided", async () => {
    const opts = {};
    let error;

    try {
        await SwaggerTools.bundleToFile(SAMPLE_SWAGGER_FILE_PATH, opts);
    } catch (e) {
        error = e;
    }

    expect(error).toBeDefined();
    expect(error.message).toBe("Please provide an output file path");
});

test("seed", async () => {
    await SwaggerTools.seed();
});

function assertBundledSwagger(bundledSwagger) {
    expect(bundledSwagger).toBeDefined();
    expect(bundledSwagger.components.schemas.CommonUser).toBeInstanceOf(Object);
    expect(
        bundledSwagger.components.schemas.AnchorAnimalCommonProperties
    ).toBeUndefined();
    expect(bundledSwagger.components.schemas.Perro).toBeInstanceOf(Object);
    expect(bundledSwagger.components.schemas.Gato).toBeInstanceOf(Object);

    expect(
        bundledSwagger.components.schemas.Perro.properties.is_a_good_boy
    ).toBeInstanceOf(Object);
    expect(
        bundledSwagger.components.schemas.Gato.properties.is_a_dick
    ).toBeInstanceOf(Object);
}
