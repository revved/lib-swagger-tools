# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/revved/lib-swagger-tools/compare/v1.1.3...v1.2.0) (2020-09-15)


### Features

* make phone number schema nullable ([5b8d288](https://gitlab.com/revved/lib-swagger-tools/commit/5b8d288))



### [1.1.3](https://gitlab.com/revved/lib-swagger-tools/compare/v1.1.2...v1.1.3) (2019-08-08)


### Bug Fixes

* do not preserve yaml anchor in swagger after building ([5e29872](https://gitlab.com/revved/lib-swagger-tools/commit/5e29872))



### [1.1.2](https://gitlab.com/revved/lib-swagger-tools/compare/v1.1.1...v1.1.2) (2019-08-06)


### Bug Fixes

* fixed type for common zip def ([8bfe0fe](https://gitlab.com/revved/lib-swagger-tools/commit/8bfe0fe))



### [1.1.1](https://gitlab.com/revved/lib-swagger-tools/compare/v1.1.0...v1.1.1) (2019-07-30)


### Bug Fixes

* include dist in files ([e5ab947](https://gitlab.com/revved/lib-swagger-tools/commit/e5ab947))



## 1.1.0 (2019-07-30)


### Bug Fixes

* Added sample under bin during pre publish step ([630b4a4](https://gitlab.com/revved/lib-swagger-tools/commit/630b4a4))
* build swagger in right dist from tests ([6345574](https://gitlab.com/revved/lib-swagger-tools/commit/6345574))
* Deleted dup sample folder under bin ([345c516](https://gitlab.com/revved/lib-swagger-tools/commit/345c516))
* Do not use fs.copy ([bff87f4](https://gitlab.com/revved/lib-swagger-tools/commit/bff87f4))
* Fixed build process ([feff0bf](https://gitlab.com/revved/lib-swagger-tools/commit/feff0bf))
* Fixed tests ([31c3dce](https://gitlab.com/revved/lib-swagger-tools/commit/31c3dce))
* Fixing sample bs ([7788b13](https://gitlab.com/revved/lib-swagger-tools/commit/7788b13))
* Fixing sample bs ([acffef6](https://gitlab.com/revved/lib-swagger-tools/commit/acffef6))
* Fixing sample bs ([54798a9](https://gitlab.com/revved/lib-swagger-tools/commit/54798a9))
* Fixing sample bs ([37b96e3](https://gitlab.com/revved/lib-swagger-tools/commit/37b96e3))
* Fixing sample bs ([45e0b07](https://gitlab.com/revved/lib-swagger-tools/commit/45e0b07))
* Fixing sample bs ([ce9f05b](https://gitlab.com/revved/lib-swagger-tools/commit/ce9f05b))
* make sure swagger.json is not tracked ([0de570c](https://gitlab.com/revved/lib-swagger-tools/commit/0de570c))
* Remove swagger folder ([6a1e102](https://gitlab.com/revved/lib-swagger-tools/commit/6a1e102))
* Remove swagger folder ([e636ae3](https://gitlab.com/revved/lib-swagger-tools/commit/e636ae3))
* Removed additional props for common definitions ([382a985](https://gitlab.com/revved/lib-swagger-tools/commit/382a985))
* Removed browser prop from package.json ([073ebf7](https://gitlab.com/revved/lib-swagger-tools/commit/073ebf7))
* Removed sample copy from prepubublish ([2409e19](https://gitlab.com/revved/lib-swagger-tools/commit/2409e19))
* Trying to fix missing samples ([29f0420](https://gitlab.com/revved/lib-swagger-tools/commit/29f0420))
* Use deferance ([dc4a977](https://gitlab.com/revved/lib-swagger-tools/commit/dc4a977))


### Features

* Default bundle in and out ([568fb8c](https://gitlab.com/revved/lib-swagger-tools/commit/568fb8c))
* Handle unknown commands & add help ([90b32e6](https://gitlab.com/revved/lib-swagger-tools/commit/90b32e6))
* seed command ([250b442](https://gitlab.com/revved/lib-swagger-tools/commit/250b442))
* Started working on seed command ([74f5f13](https://gitlab.com/revved/lib-swagger-tools/commit/74f5f13))
