#!/usr/bin/env node

// Examples:
// - ./bin/cli.js bundle sample/src/swagger.yaml sample/dist/swagger.json
// - ./bin/cli.js seed

import program from "commander";
import chalk from "chalk";
import SwaggerTools from "../lib/SwaggerTools";

program
    .command("bundle [input] [output]")
    .action(async (input, output, cmd) => {
        const inputD = input || "./swagger/src/swagger.yaml";
        const outputD = output || "./swagger/dist/swagger.json";

        const options = {
            outputFilePath: outputD
        };
        await SwaggerTools.bundleToFile(inputD, options);
    });

program.command("seed").action(async () => {
    await SwaggerTools.seed();
});

program.parse(process.argv);

// Handle unknown commands
program.on("command:*", () => {
    let errMsg = `Invalid command: ${program.args.join(" ")}\n`;
    errMsg += "See --help for a list of available commands.";
    const error = new Error(errMsg);
    handleError(error);
});

program.parse(process.argv);
if (!program.args.length) {
    program.help();
}

process.on("uncaughtException", err => {
    handleError(err);
});

function handleError(e) {
    console.log(chalk.red(e.message));
    process.exit(e);
}
